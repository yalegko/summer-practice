from flask import Flask


# Create application using provided config
from project.config import DebugConfig as Config
app = Flask(__name__)
app.config.from_object(Config)


# Initialize access control
from project.abac import AccessController
app.access_controller = AccessController()


# Get the routes back (probably not a best way but it works)
from project.views import app