# Simple aNgine demo project

Here you can find very straightforward demo of the [aNgine](https://github.com/PositiveTechnologies/angine) usage in the python 
Flask webframework.

# Requirements
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker-Compose](https://docs.docker.com/compose/install/)

# Preparation
1. Prepare config:
    ```
        $ cp project.env.example project.env
        $ vim project.env

    ```

2. Run predeploy script which prepares web template
    ```
        $ bash ./predeploy.sh
    ```

3. Build containers
    ```
        $ docker-compose build
    ```

# Running
```
    $ docker-compose up
```